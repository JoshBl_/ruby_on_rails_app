class Micropost < ApplicationRecord
# ApplicationRecord inherits from ActiveRecord::Base - allows the model object to communicate with the database, treat the database columns as Ruby attributes etc
  #part of defining the one to many relationship
  belongs_to :user
  #this will ensure that the content of each micropost is less than 140 characters
  
  validates :content, length: { maximum: 140 },
                              presence: true
end
