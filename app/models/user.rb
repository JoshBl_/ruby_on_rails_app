#this class inherits from ApplicationRecord - which arranges for Users.all to get all users from the database.
# ApplicationRecord inherits from ActiveRecord::Base - allows the model object to communicate with the database, treat the database columns as Ruby attributes etc
class User < ApplicationRecord
  #this defines a one to many relationship
  has_many :microposts
  validates :name, presence: true
  validates :email, presence: true
end
