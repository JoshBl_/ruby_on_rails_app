#ApplicationController class inherits from ActionController::Base - the base class for controllers provided by the Rails library action pack
class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  #this was created by implementing the users data model with a web interface to the model.
  #users resource - allows us to think of users as objects that can be created, updated, read and deleted via the HTTP protocol
  #users resource is created by a scaffold generator program - this comes standard with each Rails porject.
  #rails generate scaffold User name:string email:string
  #the main argument is the singular version of the resource name, in this case User
  #with optional parameters for the data model's attributes.
  
  #need to migrate the database using - rails db:migrate
  #this updates the database with our new data model (users)

  # GET /users
  # GET /users.json
  def index
    #asks the user model to retrieve all the users in the database and places them in the variable, @users
    #the @ symbol means this is an instance variable - these are available in the view (index.html.erb)
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email)
    end
end
