Rails.application.routes.draw do
  #this creates a mapping of user URLs to controller actions for the resource
  #sets up the table of URL/action pairs
  resources :microposts
  resources :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  #this will go to the index action in the users controller
  root 'users#index'
end
